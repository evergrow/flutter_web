import 'package:flutter/material.dart';
import 'package:easy_sidemenu/easy_sidemenu.dart';
import 'package:flutter_dropzone/flutter_dropzone.dart';
import 'package:flutter_web/src/exceptions.dart';
// import 'package:file_picker/file_picker.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'EDO',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Web App CI from Gitlab'),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  late DropzoneViewController controller1;
  late DropzoneViewController controller2;
  String message1 = 'Drop something here';
  String message2 = 'Drop something here';
  bool highlighted1 = false;
  PageController page = PageController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        centerTitle: true,
      ),
      body: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SideMenu(
            controller: page,
            style: SideMenuStyle(
              displayMode: SideMenuDisplayMode.auto,
              hoverColor: Colors.blue[100],
              selectedColor: Colors.lightBlue,
              selectedTitleTextStyle: TextStyle(color: Colors.white),
              selectedIconColor: Colors.white,
              // decoration: BoxDecoration(
              //   borderRadius: BorderRadius.all(Radius.circular(10)),
              // ),
              // backgroundColor: Colors.blueGrey[700]
            ),
            title: Column(
              children: [
                ConstrainedBox(
                  constraints: BoxConstraints(
                    maxHeight: 150,
                    maxWidth: 150,
                  ),
                  child: Image.asset(
                    'images/img_1.png',
                    // 'assets/images/easy_sidemenu.png',
                  ),
                ),
                Divider(
                  indent: 8.0,
                  endIndent: 8.0,
                ),
              ],
            ),
            footer: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'Flutter Tester',
                style: TextStyle(fontSize: 15),
              ),
            ),
            items: [
              SideMenuItem(
                priority: 0,
                title: 'Dashboard',
                onTap: () {
                  page.jumpToPage(0);
                },
                icon: Icon(Icons.window),
                badgeContent: Text(
                  '3',
                  style: TextStyle(color: Colors.white),
                ),
              ),
              SideMenuItem(
                priority: 1,
                title: 'Tasks',
                onTap: () {
                  page.jumpToPage(1);
                },
                icon: Icon(Icons.task_outlined),
              ),
              SideMenuItem(
                priority: 2,
                title: 'Data Management',
                onTap: () {
                  page.jumpToPage(2);
                },
                icon: Icon(Icons.dataset_outlined),
              ),
              SideMenuItem(
                priority: 3,
                title: 'Data Analysis',
                onTap: () {
                  page.jumpToPage(3);
                },
                icon: Icon(Icons.analytics),
              ),
              SideMenuItem(
                priority: 4,
                title: 'ML/AI Modeling',
                onTap: () {
                  page.jumpToPage(4);
                },
                icon: Icon(Icons.developer_mode),
              ),
              SideMenuItem(
                priority: 5,
                title: 'Artifacts',
                onTap: () {
                  page.jumpToPage(5);
                },
                icon: Icon(Icons.download),
              ),
              SideMenuItem(
                priority: 6,
                title: 'Settings',
                onTap: () {
                  page.jumpToPage(6);
                },
                icon: Icon(Icons.settings),
              ),
              SideMenuItem(
                priority: 7,
                title: 'Exit',
                onTap: () async {},
                icon: Icon(Icons.exit_to_app),
              ),
            ],
          ),
          Expanded(
            child: PageView(
              controller: page,
              children: [
                Container(
                  color: Colors.white,
                  child: Center(
                    child: Image.asset(
                      'images/img_2.png',
                      ),
                  ),
                ),
                Container(
                  color: Colors.white,
                  child: Center(
                    child: Text(
                      'Tasks',
                      style: TextStyle(fontSize: 35),
                    ),
                  ),
                ),
                Container(
                  color: Colors.white,
                  child: Center(
                    child: Text(
                      'Data Management',
                      style: TextStyle(fontSize: 35),
                    ),
                  ),
                ),
                Container(
                  color: Colors.white,
                  child: Row(
                    children: [
                      Flexible(
                        flex: 2,
                        child: Column(
                            children: [
                              Flexible(
                                child: Container(
                                  color: highlighted1 ? Colors.red : Colors.transparent,
                                  child: Stack(
                                    children: [
                                      Column(
                                        children: [
                                          Flexible(
                                            flex: 2,
                                            child: Container(
                                              margin: EdgeInsets.all(10),
                                              padding: EdgeInsets.all(5),
                                              decoration: BoxDecoration(
                                                color: Colors.black12,
                                                borderRadius: BorderRadius.all(Radius.circular(20)),
                                              ),
                                              alignment: Alignment.bottomCenter,
                                              child: Column(
                                                children: [
                                                  Flexible(
                                                    child: buildZone1(context),
                                                  ),
                                                  Center(
                                                      child: Text(message1)),
                                                  ElevatedButton.icon(
                                                    onPressed: () async {
                                                      print(await controller1.pickFiles(mime: ['image/jpeg', 'image/png']));
                                                    },
                                                    icon: Icon(Icons.add, size:18),
                                                    label: const Text('Pick file'),
                                                    style: ElevatedButton.styleFrom(
                                                      padding: EdgeInsets.all(20),
                                                    )
                                                  ),
                                                ]
                                              ),
                                            )
                                          ),
                                          Flexible(
                                            flex: 6,
                                            child: Center(
                                                child: Text('Test'))
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ),

                            ],
                          ),
                        ),

                      Flexible(
                        flex: 8,
                        child: Text(
                          'Managed Data',
                          style: TextStyle(fontSize: 35),
                        ),
                      ),
                    ],
                  ),

                  ),

                Container(
                  color: Colors.white,
                  child: Center(
                    child: Text(
                      'ML/AI Modeling',
                      style: TextStyle(fontSize: 35),
                    ),
                  ),
                ),
                Container(
                  color: Colors.white,
                  child: Center(
                    child: Text(
                      'Artifacts',
                      style: TextStyle(fontSize: 35),
                    ),
                  ),
                ),
                Container(
                  color: Colors.white,
                  child: Center(
                    child: Text(
                      'Settings',
                      style: TextStyle(fontSize: 35),
                    ),
                  ),
                ),
              ],
            ),
          ),
      ]),
    );

  }

  Widget buildZone1(BuildContext context) => Builder(
    builder: (context) => DropzoneView(
      operation: DragOperation.copy,
      cursor: CursorType.grab,
      onCreated: (ctrl) => controller1 = ctrl,
      onLoaded: () => print('Zone 1 loaded'),
      onError: (ev) => print('Zone 1 error: $ev'),
      onHover: () {
        setState(() => highlighted1 = true);
        print('Zone 1 hovered');
      },
      onLeave: () {
        setState(() => highlighted1 = false);
        print('Zone 1 left');
      },
      onDrop: (ev) async {
        print('Zone 1 drop: ${ev.name}');
        setState(() {
          message1 = '${ev.name} dropped';
          highlighted1 = false;
        });
        final bytes = await controller1.getFileData(ev);
        print(bytes.sublist(0, 20));
      },
      onDropMultiple: (ev) async {
        print('Zone 1 drop multiple: $ev');
      },
    ),
  );


}